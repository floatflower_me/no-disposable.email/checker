
addEventListener('fetch', event => {
    event.respondWith(handleRequest(event.request))
})

/**
 * @param {Request} request
 */
async function handleRequest(request) {
    const domains = require("./domains.json");
	console.log(domains);
    const url = new URL(request.url);
    const domain = url.pathname.substr(7);
    const responseBody = JSON.stringify({
        disposable: domains.includes(domain)
    });
    return new Response(responseBody, {
        headers: {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*'
        }
    });
}
