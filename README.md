No Disposable Email
===

+ Maintainer: [FloatFlower.Huang](https://github.com/floatflower) 
+ Email: floatflower1029@gmail.com

### Usage
#### All you need to do is call the API: 
https://no-disposable.email/check/{domain}

### Example
#### Request
```
GET https://no-disposable.email/check/sharklasers.com
```

#### Response
```json
{
  "disposable": true
}
```

### Why use no-disposable.email
#### Totally Open Source
Everyone who find a new disposable email services
can open an issue to request no-disposable.email list them. <br/>
We will update the new list every week after confirm the domain is a disposable email service.

#### Will never collect any data.
All source code is public, it is a every simple application, <br/> 
just check whether the domain is listed, <br/>
so you can feel free to send any email to this API, <br/>
we will never collect the email, request origin and any data sent to this service. 

#### Very fast
As you can see, this API was deployed to Cloudflare worker, <br/>
Cloudflare worker is a serverless service provides developer to push their code to network edge, therefore, no matter where you call this API, Cloudflare worker will provide you the very short latency.
