const fs = require('fs');
const CloudWorker = require('@dollarshaveclub/cloudworker');

const assert = require('assert');

const domain = require('../domain.json');

describe('no-disposable.email test', function() {
    let cloudWorker;

    before(async function () {
        const workerContext = fs.readFileSync('dist/worker.js', 'utf8');
        const DISPOSABLE_EMAIL = new CloudWorker.KeyValueStore();
        domain.map(element => DISPOSABLE_EMAIL.put(element.key, element.value));
        cloudWorker = new CloudWorker(workerContext, {bindings: {DISPOSABLE_EMAIL}});
        Object.assign(global, cloudWorker);
    });

    it('response disposable: true when domain is disposable email.', async function () {
        let url = new URL('https://no-disposable.email/check/027168.com')
        let req = new CloudWorker.Request(url)
        let res = await cloudWorker.dispatch(req)
        let body = await res.json()
        assert.equal(body.disposable, true, "This domain is disposable but return false.");
    })


    it('response disposable: true when domain is not disposable email.', async function () {
        let url = new URL('https://no-disposable.email/check/gmail.com')
        let req = new CloudWorker.Request(url)
        let res = await cloudWorker.dispatch(req)
        let body = await res.json()
        assert.equal(body.disposable, false, "This domain is not disposable but return true.");
    })

})